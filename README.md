# branch-ci-cd-poc

## Introduction

Testing out various branching and CI/CD methods.

This repository has the following settings

* Application is published as a Docker image
* Testing, Staging, and Production environment
* Each environment has it's own branch
* I'm not going to deploy any artifacts. That's outside the scope.

## Goals

### Build/Publish Once (or maybe more than once)

I would like to only build/publish an artifact once, then reuse the image in subsequent environments. I can do that by not allowing any commits directly into master, staging, or prod; a feature branch must be created, than an MR raised and merged.

This restriction allows the following.

* Build and publish of an image is only done on the feature branch. The image is tagged with the commit SHA.
* Master, staging, production branches CI job pulls the image by a commit SHA. If it doesn't exist (which means a feature branch was not successfully merged), then the job fails.

## Scenarios

### A
